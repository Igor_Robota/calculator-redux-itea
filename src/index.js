import {createStore} from "redux";
import {reduser} from "./reduser";
import {operant1, operant2, saveRez, sings} from "./action";
import colcule from "./metod/cacule";

const keys = document.querySelector(".keys");

keys.addEventListener("click", (event) => {
    if (/[0-9.]/g.test(event.target.value)){
        if (store.getState().sings) {
            store.dispatch(operant2(store.getState().operant2, event.target.value))
        } else {
            store.dispatch(operant1(store.getState().operant1, event.target.value))
        }
    } else if (/[*/+-]/.test(event.target.value)) {
        store.dispatch(sings(event.target.value))
    }
    if (store.getState().operant2) {
        document.querySelector("#diz").disabled = false;
    }
    if (/[=]/.test(event.target.value)) {
        store.dispatch(saveRez(colcule(store.getState())))
    }
})

const store = createStore(reduser)

store.subscribe(() => {
    console.log(store.getState());
});

